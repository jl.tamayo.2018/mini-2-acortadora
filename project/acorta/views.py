from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from .models import Urls
import random

# Create your views here.
@csrf_exempt
def index(request):
    urls_list = Urls.objects.all()
    #Si el metodo es POST
    if request.method == "POST":
        url = request.POST['url']
        shorturl = request.POST['url_corta']
        new_url = checkRequestBegins(url) # añado http:// si no está
        try:
            url_obj = Urls.objects.get(original=new_url)

            #context = {'url': url_.original}
            urls_list = Urls.objects.all()
            context = {'urls_list': urls_list}
            return render(request, 'content.html', context)
        except Urls.DoesNotExist:
            if not shorturl: # Si no se ha introducido un url corta
                count = Urls.objects.count() + 1
                shorturl = str(count)

            c = Urls(original=new_url, corta=shorturl)
            c.save()
            urls_list = Urls.objects.all()
            context = {'urls_list': urls_list}
        return render(request, 'content.html', context)
    #Si el metodo es GET
    elif request.method == "GET":
        context = {'urls_list': urls_list}
        return render(request, 'content.html', context)


def checkRequestBegins(url):
    if ((url[:7] != 'http://') and (url[:8] != 'https://')):  # añado en caso de que no http la url
        new_url = ("https://" + url)
    else:
        new_url = url
    return new_url


@csrf_exempt
def redirect(request, corta):
    if request.method == "GET":
        try:
            url_corta = Urls.objects.get(corta=corta)
            context = {'url': url_corta.original}
            return render(request, 'redirect.html', context, status=301)
        except Urls.DoesNotExist:
            urls_list = Urls.objects.all()
            context = {'urls_list': urls_list}
            return render(request, 'error.html', context, status=404)
